import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-player',
  templateUrl: './add-player.component.html',
  styleUrls: ['./add-player.component.scss']
})
export class AddPlayerComponent implements OnInit {

  name = new FormControl('', [Validators.required, Validators.minLength(3)]);
  age = new FormControl('', [Validators.min(15), Validators.max(35), Validators.required]);

  getErrorMessage() {
    return this.name.hasError('required') ? 'You must enter a value' : this.name.hasError('minlength') ? 'Name has to be longer than 3 chars' : '';
  }

  getAgeErrorMessage() {

    return this.age.hasError('required') ? 'age is mandatory' : this.age.hasError('min') ? 'age min value is 15' : this.age.hasError('max') ? 'age max value is 35' : '';
  }

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.name.errors);
    if(this.name.errors === null && this.age.errors === null) alert('submitted');
    else alert('errors found..');
  }

}
